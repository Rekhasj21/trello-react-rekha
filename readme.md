
<h1 align="center">Trello API</h1>

## Table of Contents

- [Overview](#overview)
- [Project Link](#project-link)
- [Built With](#built-with)
- [How to use](#how-to-use)
- [Contact](#contact)

## Overview

<a href="" rel="home text"><img src="public/trelloHomePage.png" alt="home page" width="400" height="300"/></a>

**Boards**

- Display Boards
- Creates new board
- Delete Board

<a href="" rel="image text"><img src="public/trelloListPage.png" alt="cards page"  
width="400" height="300"/></a>

**Lists**

- Display all lists in a board
- Creates a new list
- Delete/Archive a list

<a href="" rel="image text"><img src="public/trelloCardPage.png" alt="cards page"  
width="200" height="300"/></a>

**Cards**

- Display all cards in a list
- Create a new card in a list
- Delete a card

<a href="" rel="image text"><img src="public/trelloChecklistandItem.png" alt="cards page"  
width="200" height="300"/></a>

**Checklists**

- Display all checklists in a card
- Create a checklist in a card
- Delete a checklist

**Checkitems**

- Display all checkitems
- Create a new checkitem in a checklist
- Delete a checkitem
- Check a checkitem
- Uncheck a checkitem

## Project Link

<a href="https://rekha-sj-trello-api-app.netlify.app/" >Click To Check Project Link</a>

### Built With

- HTML
- Bootstrap
- React Hooks
- React Router

## How To Use

To clone and run this application, you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
$ git clone git@gitlab.com:Rekhasj21/new-todo-project.git

# Install dependencies
$ npm install

# Run the app
$ npm start
```

## Contact

- Website (<https://rekha-sj-trello-api-app.netlify.app/>)
- GitHub (<https://gitlab.com/Rekhasj21/trello-react-rekha>)
