import React from 'react'
import { Link } from 'react-router-dom'
import { AiFillDelete, AiOutlineStar } from 'react-icons/ai'

function DisplayBoards(props) {
    const { board, getFavouriteList, onClickDelete } = props
    const { name, id } = board

    const handleDelete = () => {
        onClickDelete(id)
    }
    const handleFavourite = () => {
        getFavouriteList(board)
    }

    return (
        <div className="card bg-light text-white w-25 m-2">
            <Link className='text-white' to={`${id}`}>
                <img src="/backgroundimage.jpg" className="card-img" alt="image" />
                <h5 className="card-title position-absolute top-0 start-10 fs-5">{name}</h5>
            </Link>
            <div className="d-flex justify-content-between align-items-end">
                <button className='bg-transparent border-0' type='button' onClick={handleDelete}><AiFillDelete /></button>
                <button className='bg-transparent border-0' onClick={() => handleFavourite()}><AiOutlineStar /></button>
            </div>
        </div>
    )
}

export default DisplayBoards