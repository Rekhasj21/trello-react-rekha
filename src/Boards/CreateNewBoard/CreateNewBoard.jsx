import React, { useState } from 'react'

function CreateNewBoard({ updateBoardList }) {
    const [boardTitle, setBoardTitle] = useState('')

    const submitCreateNewBoard = (event) => {
        event.preventDefault()
        updateBoardList(boardTitle)
    }

    const handleBoardTitleChange = (event) => {
        setBoardTitle(event.target.value)
    }

    return (
        <>
            <div className="card bg-light text-white w-25 m-2 border-0">
                <div className="card-img-overlay d-flex justify-content-center align-items-center" data-bs-toggle="modal"
                    data-bs-target="#exampleModal">
                    <h5 className="card-title text-black-50">Create new board</h5>
                </div>
            </div>

            <div className="modal fade" id="exampleModal" tabIndex={-1} aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h1 className="modal-title fs-5" id="exampleModalLabel">Create Board</h1>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" />
                    </div>
                    <form className="modal-body" onSubmit={submitCreateNewBoard}>
                        <div className="mb-3">
                            <label htmlFor="boardTitle" className="form-label">Board Title</label>
                            <input type="text" className="form-control" id="boardTitle" value={boardTitle} onChange={handleBoardTitleChange} />
                        </div>
                        <div className='mb-3'>
                            <label className="form-label">Visibility</label>
                            <select className="form-select mb-3">
                                <option value="workspace">Workspace</option>
                                <option value="public">Public</option>
                                <option value="private">Private</option>
                            </select>
                        </div>
                        <div className="modal-footer">
                            <button type="submit" className="btn btn-primary" data-bs-dismiss="modal">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </>
    )
}

export default CreateNewBoard
