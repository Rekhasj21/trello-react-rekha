import React, { useEffect, useState } from 'react'
import BoardsApi, { CreateApi, DeleteCard } from '../../Servers/BoardsApi'
import DisplayBoards from '../DisplayBoards/DisplayBoards'
import { BsPerson } from 'react-icons/bs'
import CreateNewBoard from '../CreateNewBoard/CreateNewBoard'


function Boards() {
    const [boardsList, setBoardsList] = useState([])
    const [isLoading, setLoader] = useState(true)
    const [favouriteList, setFavouriteList] = useState([])

    useEffect(() => {
        BoardsApi().then(data => setBoardsList(data.data))
        setLoader(false)
    }, [])

    const updateBoardList = async (boardTitle) => {
        const boardData = await CreateApi(boardTitle)
        setBoardsList(prev => [...prev, boardData.data])
    }

    const getFavouriteList = (favouriteBoard) => {
        setFavouriteList(prevBoard => [...prevBoard, favouriteBoard])
    }

    const onClickDelete = (id) => {
        DeleteCard(id)
        const updatedListAfterDeleteBoard = boardsList.filter(board => board.id !== id)
        setBoardsList(updatedListAfterDeleteBoard)
    }
    
    return (
        <>
            {favouriteList.length > 0 ? (
                <div>
                    <div className='d-flex justify-content-start align-items-center' >
                        <BsPerson className='fs-3' />
                        <h1 className='fs-4 p-2'>Starred boards</h1>
                    </div>
                    <div className='d-flex flex-wrap'>
                        {favouriteList.map(board => (
                            <DisplayBoards key={board.id} board={board} />
                        ))}
                    </div>
                </div>
            ) : ('')
            }
            <div className='d-flex justify-content-start align-items-center' >
                <BsPerson className='fs-3' />
                <h1 className='fs-4 p-2'>Personal Boards</h1>
            </div>
            <div className='d-flex flex-wrap'>

                {isLoading ? (<div className="d-flex justify-content-center">
                    <div className="spinner-border" role="status">
                        <span className="visually-hidden">Loading...</span>
                    </div>
                </div>
                ) : (boardsList && boardsList.map(board => (
                    <DisplayBoards key={board.id} board={board} onClickDelete={onClickDelete} getFavouriteList={getFavouriteList} />
                )))}
                {!isLoading && < CreateNewBoard updateBoardList={updateBoardList} />}
            </div>
        </>
    )
}

export default Boards