import React from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import BoardsHomePage from './Boards/BoardsHomePage/BoardsHomePage'
import ListsInBoard from './Lists/ListsInBoard/ListsInBoard'
import NavBar from './Header/NavBar'

function App() {

  return (
    <BrowserRouter>
      <NavBar />
      <Routes>
        <Route path='/' element={<BoardsHomePage />} />
        <Route path='/:id' element={<ListsInBoard />} />
      </Routes>
    </BrowserRouter>
  )
}

export default App