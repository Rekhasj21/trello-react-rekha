import React, { useState } from 'react'
import { RxPencil1 } from 'react-icons/rx'
import { FiEdit } from 'react-icons/fi'
import CheckList from '../CheckList/CheckList'

function DisplayCards({ card, onClickDeleteCard }) {
  const { id, name } = card
  const [checklistName, setChecklistName] = useState('')
  const [isDelete, setDelete] = useState(false)

  const handleDeleteCard = () => {
    onClickDeleteCard(id)
  }

  return (
    <>
      <div className='d-flex justify-content-around align-items-center shadow p-1 m-3 bg-light rounded '>
        <button className='btn w-75' data-bs-toggle="modal" data-bs-target={`#staticBackdrop${id}`} onClick={() => setChecklistName(name)}>
          <h1 className='fs-6'>{name}</h1>
        </button>
        <button className='btn' onClick={() => setDelete(prev => !prev)}><RxPencil1 className='fs-7' /></button>
        {isDelete && <button type='button' className='btn border-0 bg-transparent' onClick={handleDeleteCard}>
          Delete
        </button>}
      </div>

      <div className="modal fade modal-lg" id={`staticBackdrop${id}`} data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div className="modal-dialog ">
          <div className="modal-content">
            <div className="modal-header">
              <h1 className="modal-title fs-5" id="staticBackdropLabel">{checklistName}</h1>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="d-flex modal-body">
              <div className='w-75'>
                <CheckList cardId={id} />
              </div>
              <div className='d-flex flex-column align-items-center w-25'>
                <h1 className='fs-6'>Add to Card</h1>
                <button className='btn bg-light'> <FiEdit /> Checklist</button>
                <button className='btn bg-light'> <FiEdit /> CheckItem</button>

              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default DisplayCards