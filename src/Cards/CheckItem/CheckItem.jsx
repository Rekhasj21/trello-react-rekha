import React, { useEffect, useState } from 'react'
import { getCheckItems, deleteCheckItem, createCheckItem } from '../../Servers/BoardsApi'
import DisplayCheckItem from '../DisplayCheckItem/DisplayCheckItem'

function CheckItem({ id }) {
    const [checkItems, setCheckItems] = useState([])

    useEffect(() => {
        getCheckItems(id).then(response => setCheckItems(response.data))
    }, [])

    const onClickCheckItemDelete = (checkItemId) => {
        deleteCheckItem(id, checkItemId)
        setCheckItems(prev => prev.filter(items => items.id !== checkItemId))
    }

    const onClickAddCheckItem = (checkItemTitle) => {
        createCheckItem(checkItemTitle, id).then(response => setCheckItems(prev => [...prev, response.data]))

    }
    return (
        <>
            {checkItems && checkItems.map(Checkitem => (
                <DisplayCheckItem key={Checkitem.id} Checkitem={Checkitem} onClickAddCheckItem={onClickAddCheckItem} onClickCheckItemDelete={onClickCheckItemDelete} />
            ))}
        </>
    )
}

export default CheckItem