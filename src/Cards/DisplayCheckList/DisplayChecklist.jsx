import React, { useState, useEffect } from 'react'
import { getCheckItems, deleteCheckItem, createCheckItem, updateCheckItem } from '../../Servers/BoardsApi'
import DisplayCheckItem from '../DisplayCheckItem/DisplayCheckItem'
import { FiEdit } from 'react-icons/fi'

function DisplayChecklist({ cardId, list, onClickDeleteCheckList }) {
    const { name, id } = list
    const [isAddCheckItem, setAddCheckItem] = useState(false)
    const [itemName, setItemName] = useState('')
    const [checkItems, setCheckItems] = useState([])

    const completedCount = checkItems.filter(item => item.state === 'complete').length;
    const totalCount = checkItems.length;
    const percentage = Math.round((completedCount / totalCount) * 100);
    const progressStyle = { width: `${percentage}%` };

    const handleDeleteCheckList = () => {
        onClickDeleteCheckList(id)
    }

    const handleAddCheckItem = () => {
        onClickAddCheckItem(itemName)
        setItemName('')
    }

    useEffect(() => {
        getCheckItems(id).then(response => setCheckItems(response.data))
    }, [])

    const onClickCheckItemDelete = (checkItemId) => {
        deleteCheckItem(id, checkItemId)
        setCheckItems(prev => prev.filter(items => items.id !== checkItemId))
    }

    const onClickAddCheckItem = (checkItemTitle) => {
        createCheckItem(checkItemTitle, id).then(response => setCheckItems(prev => [...prev, response.data]))
    }

    const onClickCheckbox = (checkboxId, currentState) => {
        const newState = currentState === "complete" ? "incomplete" : "complete";
        const updatedCheckItems = checkItems.map(item => {
            if (item.id === checkboxId) {
                return {
                    ...item,
                    state: newState
                };
            }
            return item;
        });
        setCheckItems(updatedCheckItems);
        updateCheckItem(cardId, checkboxId, newState)
    }

    return (
        <>
            <div className='d-flex justify-content-between align-items-center'>
                <button className='bg-body border-0 m-1'><FiEdit className='fs-6 m-2' />{name} </button>
                <button className='btn bg-light' onClick={handleDeleteCheckList}>Delete</button>
            </div>
            <div className="progress m-2">
                <div className="progress-bar" role="progressbar" aria-label="Basic example"
                    style={progressStyle} aria-valuenow={percentage} aria-valuemin={0} aria-valuemax={100} />
            </div>

            {checkItems && checkItems.map(Checkitem => (
                <DisplayCheckItem key={Checkitem.id} Checkitem={Checkitem} onClickCheckbox={onClickCheckbox} onClickAddCheckItem={onClickAddCheckItem} onClickCheckItemDelete={onClickCheckItemDelete} />
            ))}

            {isAddCheckItem &&
                (<div className="form-floating m-1">
                    <textarea className="form-control" value={itemName} placeholder="Add an Item" id="floatingTextarea" onChange={() => setItemName(event.target.value)} />
                    <button className='btn btn-primary m-1' onClick={handleAddCheckItem}>Add</button>
                    <button className='btn bg-light' onClick={() => setAddCheckItem(prev => !prev)}>Cancel</button>
                </div>)}
            <button className='btn bg-light' onClick={() => setAddCheckItem(prev => !prev)}>Add an Item</button>
        </>
    )
}

export default DisplayChecklist