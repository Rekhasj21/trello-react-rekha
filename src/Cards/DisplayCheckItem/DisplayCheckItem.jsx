import React from 'react'

function DisplayCheckItem({ Checkitem, onClickCheckItemDelete ,onClickCheckbox}) {
    const { name, id , state } = Checkitem

    const handleDeleteCheckItem = () => {
        onClickCheckItemDelete(id)
    }
    const onhandleCheckbox=()=>{
        onClickCheckbox(id, state)
    }

    return (
        <ul className="list-group">
            <li className="d-flex justify-content-between list-group-item m-1" key={id}>
                <div>
                    <input className="form-check-input me-1"
                        type="checkbox" defaultValue="" id={id} checked={state === "complete"} onChange={onhandleCheckbox}/>
                    <label className="form-check-label" htmlFor={id}>{name} </label>
                </div>
                <button className='btn' onClick={handleDeleteCheckItem}> Delete</button>
            </li>
        </ul>
    )
}

export default DisplayCheckItem