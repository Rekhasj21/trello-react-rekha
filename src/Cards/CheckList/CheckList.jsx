import React, { useEffect, useState } from 'react'
import { getChecklist, deleteCheckList, createNewChecklist } from '../../Servers/BoardsApi'
import DisplayChecklist from '../DisplayCheckList/DisplayChecklist'

function CheckList({ cardId }) {
    const [checklist, setCheckList] = useState([])
    const [checklistName, setChecklistName] = useState('')
    const [isAddChecklist, setAddChecklist] = useState(false)

    useEffect(() => {
        getChecklist(cardId).then(response => setCheckList(response.data))
    }, [])

    const onClickDeleteCheckList = (deleteId) => {
        deleteCheckList(deleteId)
        setCheckList(prev => prev.filter(checklist => checklist.id !== deleteId))
    }

    const onAddCheckList = (checklistName) => {
        createNewChecklist(checklistName, cardId).then(response => setCheckList(prev => [...prev, response.data]))
    }

    const handleCheckListSubmit = () => {
        event.preventDefault()
        onAddCheckList(checklistName)
        setChecklistName('')
    }

    return (
        <>
            {checklist && checklist.map(list => (
                <DisplayChecklist cardId={cardId} list={list} key={list.id} onAddCheckList={onAddCheckList} onClickDeleteCheckList={onClickDeleteCheckList} />
            ))}
            {isAddChecklist && (
                <form onSubmit={handleCheckListSubmit}>
                    <div className="mb-3">
                        <label htmlFor="exampleInputEmail1" className="form-label"> Title </label>
                        <input type="text" className="form-control" placeholder='checklist'
                            id="exampleInputEmail1" aria-describedby="emailHelp" value={checklistName} onChange={() => setChecklistName(event.target.value)} />
                        <button type="submit" className="btn btn-primary m-1"> Add</button>
                    </div>
                </form>
            )}
            <br />
            <button className='btn' onClick={() => setAddChecklist(prev => !prev)}>Add CheckList</button>
        </>
    )
}

export default CheckList