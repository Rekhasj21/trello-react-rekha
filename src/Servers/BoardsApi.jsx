import axion from 'axios'

const apiKey = 'fb0e7b684ea9c6943ee0f98f6be2d480'
const token = 'ATTA0b4ce320bfde365626b3fc511c469d74f542e0b8042402e1b61e88febbd4f3b32E51DA9A'

export default function BoardsApi() {
    const boardsData = axion.get(`https://api.trello.com/1/members/me/boards?key=${apiKey}&token=${token}`)
        .then(response => response)
        .catch(error => console.log(error))
    return boardsData
}

export function CreateApi(title) {
    const newCardData = axion.post(`https://api.trello.com/1/boards/?name=${title}&key=${apiKey}&token=${token}`)
        .catch(error => console.log(error.data))
    return newCardData
}

export function DeleteCard(deleteId) {
    axion.delete(`https://api.trello.com/1/boards/${deleteId}?key=${apiKey}&token=${token}`)
        .catch(error => console.log(error))
}

export function getListsInBoard(cardId) {
    const listsInBoard = axion.get(`https://api.trello.com/1/boards/${cardId}/lists?key=${apiKey}&token=${token}`)
        .then(response => response)
        .catch(error => console.log(error.data))
    return listsInBoard
}

export function createBoardListApi(listTitle, boardId) {
    const newList = axion.post(`https://api.trello.com/1/boards/${boardId}/lists?name=${listTitle}&key=${apiKey}&token=${token}`)
        .catch(error => console.log(error))
    return newList
}

export function archiveBoardList(deleteId) {
    axion.put(`https://api.trello.com/1/lists/${deleteId}/closed?value=true&key=${apiKey}&token=${token}`)
        .catch(error => console.log(error))
}

export function addNewCardApi(cardTitle, boardId) {
    const newCardData = axion.post(`https://api.trello.com/1/cards?name=${cardTitle}&idList=${boardId}&key=${apiKey}&token=${token}`)
    return newCardData
}

export function getCardsData(id) {
    const cardsData = axion.get(`https://api.trello.com/1/lists/${id}/cards?key=${apiKey}&token=${token}`)
    return cardsData
}

export function archiveCard(deleteCardId) {
    axion.delete(`https://api.trello.com/1/cards/${deleteCardId}?key=${apiKey}&token=${token}`)
}

export function getChecklist(cardId) {
    const checkListData = axion.get(`https://api.trello.com/1/cards/${cardId}/checklists?key=${apiKey}&token=${token}`)
    return checkListData
}

export function deleteCheckList(deleteId) {
    axion.delete(`https://api.trello.com/1/checklists/${deleteId}?key=${apiKey}&token=${token}`)
}

export function createNewChecklist(checklistName, cardId) {
    const newCheckListData = axion.post(`https://api.trello.com/1/checklists?name=${checklistName}&idCard=${cardId}&key=${apiKey}&token=${token}`)
        .catch(error => console.log(error.message))
    return newCheckListData
}

export function getCheckItems(checkListId) {
    const checkItemsData = axion.get(`https://api.trello.com/1/checklists/${checkListId}/checkItems?key=${apiKey}&token=${token}`)
    return checkItemsData
}

export function deleteCheckItem(checkListId, checkItemId) {
    axion.delete(`https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?key=${apiKey}&token=${token}`)
}

export function createCheckItem(itemName, checkListId) {
    const newCheckItemData = axion.post(`https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${itemName}&key=${apiKey}&token=${token}`)
        .catch(error => console.log(error))
    return newCheckItemData
}
export function updateCheckItem(cardId, checkItemId, newState) {
    const newCheckList = axion.put(`https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?state=${newState}&key=${apiKey}&token=${token}`)
        .catch(error => console.log(error.message))
    return newCheckList

}