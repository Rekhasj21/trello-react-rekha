import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import DisplayListsInBoard from '../../Lists/DisplayListsInBoard/DisplayListInBoard'
import CreateNewList from '../CreateNewList/CreateNewList'
import { archiveBoardList, createBoardListApi,getListsInBoard } from '../../Servers/BoardsApi'

let isAddList = true
function ListsInBoard() {
    const { id } = useParams()
    const [boardLists, setBoardLists] = useState([])
    const [isLoading, setLoading] = useState(true)
    
    useEffect(() => {
        getListsInBoard(id).then(data => setBoardLists(data.data))
        setLoading(false)
    }, [])

    const updateBoardList = async (listTitle, boardId) => {
        const newListData = await createBoardListApi(listTitle, boardId)
        setBoardLists(prev => [...prev, newListData.data])
    }

    const onClickDeleteBoardList = (deleteId) => {
        archiveBoardList(deleteId)
        setBoardLists(prev => prev.filter(list => list.id !== deleteId))
    }
    return (
        <>
            <div className='d-flex overflow-x-scroll bg-success bg-opacity-10 '>
                {isLoading ? (<div className="d-flex justify-content-center">
                    <div className="spinner-border" role="status">
                        <span className="visually-hidden">Loading...</span>
                    </div>
                </div>
                ) :
                    (boardLists && boardLists.map(boardList => (
                        <DisplayListsInBoard key={boardList.id} boardList={boardList} onClickDeleteBoardList={onClickDeleteBoardList} isAddList={isAddList} />
                    )))
                }
                {!isLoading && isAddList && < CreateNewList boardId={id} updateBoardList={updateBoardList} />}
            </div >
        </>
    )
}

export default ListsInBoard
