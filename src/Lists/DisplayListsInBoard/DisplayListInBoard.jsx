import React, { useState, useEffect } from 'react'
import { BsThreeDots } from 'react-icons/bs'
import { addNewCardApi, getCardsData, archiveCard } from '../../Servers/BoardsApi'
import { VscFileSubmodule } from 'react-icons/vsc'
import { AiOutlinePlus } from 'react-icons/ai'
import CardsHomePage from '../../Cards/CardsHomePage/CardsHomePage'

function DisplayListInBoard({ boardList, onClickDeleteBoardList }) {
    const { id, name } = boardList
    const [isAddList, setAddList] = useState(false)
    const [cardTitle, setCardTitle] = useState('')
    const [cardList, setCardList] = useState('')
    const [isDelete, setDelete] = useState(false)

    useEffect(() => {
        getCardsData(id).then(data => setCardList(data.data))
    }, [])

    const onClickDeleteCard = (deleteCardId) => {
        archiveCard(deleteCardId)
        setCardList(prev => prev.filter(card => card.id !== deleteCardId))
    }
    const handleDeleteList = () => {
        onClickDeleteBoardList(id)
    }
    const handleCardTitle = (event) => {
        setCardTitle(event.target.value)
    }
    const handleNewCardSubmit = async (event) => {
        event.preventDefault()
        const newCardData = await addNewCardApi(cardTitle, id)
        setCardList(prev => [...prev, newCardData.data])
        setCardTitle('')
    }
    return (
        <div className='d-flex flex-column m-3 bg-secondary bg-opacity-10 h-50' style={{ width: "20rem" }}>
            <div className='d-flex justify-content-between align-items-center p-3'>
                <h5 classname="card-title"> {name} </h5>
                <button className='btn' onClick={() => setDelete(prev => !prev)}><BsThreeDots /></button>
                {isDelete && <button className='bg-transparent border-0' onClick={handleDeleteList}>Archive</button>}
            </div>

            {cardList && cardList.map(card => (
                <CardsHomePage key={card.id} card={card} onClickDeleteCard={onClickDeleteCard} />
            ))}
            {!isAddList && (<div className='d-flex justify-content-between align-items-center p-3'>
                <div className='d-flex justify-content-start align-items-center'>
                    <AiOutlinePlus />
                    <button className='bg-transparent border-0' onClick={() => setAddList(prev => !prev)}>Add a card</button>
                </div>
                <VscFileSubmodule />
            </div>)}

            {isAddList && (
                <form className="p-2" onSubmit={handleNewCardSubmit}>
                    <textarea type="text" value={cardTitle} placeholder='Enter title for this card...' className="form-control" id="boardTitle" onChange={handleCardTitle} />
                    <div className='d-flex justify-content-between'>
                        <div>
                            <button type="submit" className="btn btn-primary ">Add Card</button>
                            <button type="button" class="btn-close m-3" onClick={() => setAddList(prev => !prev)}></button>
                        </div>
                        <button className='btn ml-5'><BsThreeDots /></button>
                    </div>
                </form>
            )}
        </div>
    )
}

export default DisplayListInBoard