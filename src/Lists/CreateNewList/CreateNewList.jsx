import React, { useState } from 'react'
import { AiOutlinePlus } from 'react-icons/ai'

function CreateNewList({ boardId, updateBoardList }) {
    const [listTitle, setlistTitle] = useState('')

    const handleListTitleChange = (event) => {
        setlistTitle(event.target.value)
    }
    const handleCreateBoardList = (event) => {
        event.preventDefault()
        updateBoardList(listTitle, boardId)
        setlistTitle('')
    }
    return (
        <div className='d-flex justify-content-between align-items-start p-3'>
            <div className='d-flex justify-content-start align-items-center'>
                <AiOutlinePlus />
                <button className='bg-transparent border-0' data-bs-toggle="modal"
                    data-bs-target="#exampleModal">Add a another list</button>
            </div>

            <div className="modal fade" id="exampleModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <form className="modal-body" onSubmit={handleCreateBoardList} >
                            <div className="mb-3">
                                <label htmlFor="boardTitle" className="form-label">Board Title</label>
                                <input type="text" value={listTitle} placeholder='Enter list title' className="form-control" id="boardTitle" onChange={handleListTitleChange} />
                            </div>
                            <button type="submit" className="btn btn-primary" data-bs-dismiss="modal">Add list</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CreateNewList