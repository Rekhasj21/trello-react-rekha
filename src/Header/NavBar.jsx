import React from 'react'
import { Link } from 'react-router-dom'

function NavBar() {
    return (
        <nav className="navbar navbar-light bg-primary d-flex justify-content-evenly">
            <Link to='/'>
                <button type="button" className="btn btn-light">Boards</button>
            </Link>
            <a className="navbar-brand" href="#">
                <img src="trello-icon-png-21.jpg" alt="" width="80" height="30" className="d-inline-block align-text-center bg-light" />

            </a>
        </nav>
    )
}

export default NavBar